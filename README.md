# README #
This repository is for L2topgamer vote reward system and is adapted to L2J aCis server pack.
With this pack topgamer reward voters with skill or item (personally) and reward all players with item when required number of votes are reached. Global reward system can be disabled.


### How to set up? ###

* Clone this repository to your ide.
* Copy and paste all packets and files except Config.java, GameServer.java and ChatAll.java to your L2J acis pack.
* From files mentioned above copy code blocks that starts with "vote system start" and ends with "vote system end" to your server original files. 
* Go to L2topgamer website and copy API token of your server to vote.properties file topgamerToken variable.
* If you will chose to reward players with skills every time when player logs in game or change sub and lost a reward skill, skill can be added by using giveRewardSkill method of VoteHandler class.

### Extra files ###

* jackson-all-1.9.0.jar add this file to your gameserver lib folder and add to your project library
* vote.properties add this file to your gameserver config folder
* topgamer_voters.sql add this file to your gameserver database
### How to use ###
Player in chat types .votetopgamer and if he voted on L2topgamer website he will get reward
### aCis version difference ###
If you have older version of aCis server pack and your ide shows error on getPlayers() method, change this method with .getAllPlayers().values()
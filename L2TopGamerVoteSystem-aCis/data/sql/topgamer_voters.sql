DROP TABLE IF EXISTS `topgamer_voters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topgamer_voters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(75) NOT NULL,
  `voted_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `ip_UNIQUE` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

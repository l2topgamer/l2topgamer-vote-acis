package net.sf.l2j.gameserver.vote.model.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author L2Cygnus
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class topgamerGlobalResponse
{
	private boolean ok;
	private topgamerResultTotal result;
	
	@JsonProperty("result")
	public topgamerResultTotal getResult()
	{
		return result;
	}
	
	public void setTotalVotes(topgamerResultTotal responseResult)
	{
		result = responseResult;
	}
	
	@JsonProperty("ok")
	public boolean isOk()
	{
		return ok;
	}
	
	public void setOk(boolean isOk)
	{
		ok = isOk;
	}
}

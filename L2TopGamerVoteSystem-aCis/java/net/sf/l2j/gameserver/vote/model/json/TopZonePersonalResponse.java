package net.sf.l2j.gameserver.vote.model.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author L2Cygnus
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class topgamerPersonalResponse
{
	private boolean ok;
	private topgamerResultPersonal result;
	
	@JsonProperty("result")
	public topgamerResultPersonal getResult()
	{
		return result;
	}
	
	public void setTotalVotes(topgamerResultPersonal responseResult)
	{
		result = responseResult;
	}
	
	@JsonProperty("ok")
	public boolean isOk()
	{
		return ok;
	}
	
	public void setOk(boolean isOk)
	{
		ok = isOk;
	}
}

package net.sf.l2j.gameserver.vote.sql;

import net.sf.l2j.Config;
import net.sf.l2j.L2DatabaseFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author L2Cygnus
 */
public class VotersTable {

    public void saveVoter(String ip) {

        try (final Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            try (final PreparedStatement stmt = con.prepareStatement("INSERT INTO topgamer_voters (ip,voted_time) values(?,?)")) {
                stmt.setString(1, ip);
                stmt.setLong(2, System.currentTimeMillis());
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }

    public Map<String, Long> loadVoters() {
        Map<String, Long> voterMap = new HashMap<>();
        try (final Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            try (final PreparedStatement stmt = con.prepareStatement("SELECT ip, voted_time FROM topgamer_voters")) {
                try (final ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        voterMap.put(rs.getString("ip"), rs.getLong("voted_time"));
                    }
                }
            }
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
        System.out.println("Voters loaded: " + voterMap.size());
        return voterMap;
    }

    public void deleteVoter(String ip) {
        try (final Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            try (final PreparedStatement prep = con.prepareStatement("DELETE FROM topgamer_voters WHERE ip = ?")) {
                prep.setString(1, ip);
                prep.executeUpdate();
                prep.close();
            }
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }

    public void deleteVoters(Set<String> voters) {
        try (final Connection con = L2DatabaseFactory.getInstance().getConnection()) {
            try (final PreparedStatement prep = con.prepareStatement("DELETE FROM topgamer_voters WHERE ip = ?")) {
                for (String ip : voters) {
                    prep.setString(1, ip);
                    prep.executeUpdate();
                }
            }
        } catch (SQLException e) {
            if (Config.VOTE_LOG_ERROR) {
                e.printStackTrace();
            }
        }
    }
}
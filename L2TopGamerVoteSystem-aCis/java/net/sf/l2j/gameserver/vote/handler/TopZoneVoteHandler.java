package net.sf.l2j.gameserver.vote.handler;

import org.codehaus.jackson.map.ObjectMapper;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.vote.AbstractVoteManager;
import net.sf.l2j.gameserver.vote.model.json.topgamerGlobalResponse;
import net.sf.l2j.gameserver.vote.model.json.topgamerPersonalResponse;

/**
 * @author L2Cygnus
 */
public class topgamerVoteHandler extends AbstractVoteManager implements IVoteHandler {

	@Override
	public boolean isVoted(String ip) {
		String url = getApiUrlTemplatePersonal(ip);
		String response = getResponse(url, "L2topgamer");
		topgamerPersonalResponse resp = null;
		boolean voted = false;
		try {
			resp = new ObjectMapper().readValue(response, topgamerPersonalResponse.class);
			if (resp != null) {
				voted = resp.getResult().isVoted();
			}
		} catch (Exception e) {
			if (Config.VOTE_LOG_ERROR) {
				e.printStackTrace();
			}
			_log.warning("Failed to check personal vote status for topgamer");
		}
		return voted;
	}

	@Override
	public int getTotalVotes() {
		String url = getApiUrlTemplateTotal();
		String response = getResponse(url, "L2topgamer");
		int votes = -1;
		topgamerGlobalResponse jsonResponse = null;
		try {
			jsonResponse = new ObjectMapper().readValue(response, topgamerGlobalResponse.class);
			if (jsonResponse != null) {
				votes = jsonResponse.getResult().getTotalVotes();
			}
		} catch (Exception e) {
			if (Config.VOTE_LOG_ERROR) {
				e.printStackTrace();
			}
			_log.warning("Failed to read total votes for topgamer");
		}
		return votes;
	}

	private String getApiUrlTemplatePersonal(String ip) {
		return String.format("%svote?token=%s&ip=%s", Config.VOTE_topgamer_API_URL, Config.VOTE_TOP_ZONE_TOKEN, ip);
	}

	private String getApiUrlTemplateTotal() {
		return String.format("%sserver_%s/getServerData", Config.VOTE_topgamer_API_URL, Config.VOTE_TOP_ZONE_TOKEN);
	}
}

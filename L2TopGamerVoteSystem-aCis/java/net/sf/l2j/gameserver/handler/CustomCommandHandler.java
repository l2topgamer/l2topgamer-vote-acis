/*
 * This program is free software: topgamer redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.handler;

import java.util.HashMap;
import java.util.Map;
import net.sf.l2j.gameserver.handler.customcommandhandler.VoteCommandHandler;

/**
 * @author L2Cygnus
 */
public class CustomCommandHandler {
	private final Map<Integer, ICustomCommandHandler> _datatable = new HashMap<>();

	public static CustomCommandHandler getInstance() {
		return SingletonHolder._instance;
	}

	protected CustomCommandHandler() {
		registerCustomCommandHandler(new VoteCommandHandler());
	}

	public void registerCustomCommandHandler(ICustomCommandHandler handler) {
		for (String id : handler.getCustomCommandList())
			_datatable.put(id.hashCode(), handler);
	}

	public ICustomCommandHandler getCustomCommandHandler(String customCommand) {
		return _datatable.get(customCommand.hashCode());
	}

	public int size() {
		return _datatable.size();
	}

	private static class SingletonHolder {
		protected static final CustomCommandHandler _instance = new CustomCommandHandler();
	}
}

/*
 * This program is free software: topgamer redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.handler.customcommandhandler;

import net.sf.l2j.gameserver.handler.ICustomCommandHandler;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.vote.handler.VoteHandler;

/**
 * @author L2Cygnus
 */
public class VoteCommandHandler implements ICustomCommandHandler
{
	
	@Override
	public void useCustomCommand(String command, L2PcInstance activeChar)
	{
		if (command.equalsIgnoreCase(".votetopgamer"))
		{
			VoteHandler.getInstance().handleVote(activeChar);
		}
	}
	
	@Override
	public String[] getCustomCommandList()
	{
		return new String[]
		{
			".votetopgamer"
		};
	}
	
}
